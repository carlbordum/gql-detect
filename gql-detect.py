import argparse

import requests


PAYLOAD = {"query": "query\n{__typename\n}"}
X_WWW_FORM_HEADER = {"content-type": "x-www-form-urlencode"}
_endpoints = [
    "/",
    "/graphql",
    "/api",
    "/api/graphql",
    "/graphql/api",
    "/graphql/graphql",
]
ENDPOINTS = _endpoints + [f"{path}/v1" for path in _endpoints]


def main():
    parser = argparse.ArgumentParser(prog="gql-detect", description="Automatically detect GraphQL endpoints")
    parser.add_argument("url")
    args = parser.parse_args()

    root = args.url.removesuffix("/")

    for path in ENDPOINTS:
        url = f"{root}{path}"

        if requests.post(url, json=PAYLOAD).ok:
            print(f"POST {path} might be a GraphQL endpoint!")

        if requests.post(url, headers=X_WWW_FORM_HEADER, data=PAYLOAD).ok:
            print(f"POST {path} with {X_WWW_FORM_HEADER!r} header might be an GraphQL endpoint with CSRF vulnerabilities!")

        if requests.get(url, headers=X_WWW_FORM_HEADER, params=PAYLOAD).ok:
            print(f"GET {path} with {X_WWW_FORM_HEADER!r} header might be an GraphQL endpoint with CSRF vulnerabilities!")


if __name__ == "__main__":
    main()
